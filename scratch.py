# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 18:22:01 2018

@author: 02012018
"""

from Animal.Mammal import dog
print(dog.speak())

from Animal.Mammal import human
print(human.speak())

from Animal.Bird import eagle
print(eagle.speak())

from Animal.Bird import sparrow
print(sparrow.speak())

